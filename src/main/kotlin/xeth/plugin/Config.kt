/**
 * Config
 * 
 * Contains global config state variables read in from 
 * plugin config.yml file
 */

package xeth.plugin

import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.file.FileConfiguration

public object Config {

    // some config setting
    public var someSetting: Int = 1

    // load config, update settings
    public fun load(config: FileConfiguration) {
        Config.someSetting = config.getInt("someSetting", Config.someSetting)
    }
}