/*
 * Implement bukkit plugin interface
 */

package xeth.plugin

import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import xeth.plugin.Main
import xeth.plugin.PluginCommand
import xeth.plugin.Config

public class XethPlugin : JavaPlugin() {
    
    override fun onEnable() {
        
        // measure load time
        val timeStart = System.currentTimeMillis()

        val logger = this.getLogger()

        // get config file
        val configPath = File(this.getDataFolder().getPath(), "config.yml")
        if ( !configPath.exists() ) {
            logger.info("No config found: generating default config.yml")
            this.saveDefaultConfig()
        }
        val config = this.getConfig()
        if ( config !== null ) {
            Config.load(config)
        }

        // register listeners
        // val pm = this.getServer().getPluginManager()
        // pm.registerEvents(PluginListener(), this)

        // register commands
        this.getCommand("plugincommand")?.setExecutor(PluginCommand())

        // print load time
        val timeEnd = System.currentTimeMillis()
        val timeLoad = timeEnd - timeStart
        logger.info("Enabled in ${timeLoad}ms")

        // print success message
        logger.info("now this is epic")
    }

    override fun onDisable() {
        logger.info("wtf i hate xeth now")
    }
}
